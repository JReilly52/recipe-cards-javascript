/*Copyright 2019 J.P.Reilly
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */


/*Author: J.Reilly
 *Date: 04/18/2018
 *Function: collect and display recipes
 *
 */

//private cake counter variable
Cake.prototype.CounterNum = 0;

//used as static variable
NumberOfCakes = 4;

//cake class constructor
function Cake(Name, Flour, Sugar, Eggs, Flavor, Milk, Misc, Mixing, Cooking){
    this.Name = Name;
    this.Flour = Flour;
    this.Sugar = Sugar;
    this.Eggs = Eggs;
    this.Flavor = Flavor;
    this.Milk = Milk;
    this.Misc = Misc;
    this.Mixing = Mixing;
    this.Cooking = Cooking;
    this.CardNum = Cake.prototype.CounterNum;
    //set unique id/class tags for each object so the ratings links work on the correct object
    this.TableTag = "table" + this.CardNum;
    this.GreatTag = "great" + this.CardNum;
    this.OkayTag = "okay" + this.CardNum;
    this.EcchTag = "ecch" + this.CardNum;
    this.LinkTag = "link" + this.CardNum;
    Cake.prototype.CounterNum +=1;
}

//constructs main cake card table
Cake.prototype.showMeCakeMain = function(){
    var OutputTable;
    
    //build an output table to look like a recipe book card
    OutputTable = '<table><tbody><tr><th colspan=7><a href="javascript:void(0)" id="' + this.LinkTag + '">' + this.Name + ' Recipe</a></th></tr>';
    OutputTable += "<tr><td class=\"" + this.TableTag + "\"><u>Ingredients</u>"; //used css class tag to be able to work with both <td> nodes
    OutputTable += "<br />" + this.Flour + " flour,";
    OutputTable += "<br />" + this.Sugar + " sugar,";
    OutputTable += "<br />" + this.Eggs + " egg(s),";
    OutputTable += "<br />" + this.Flavor + " for flavoring,";
    OutputTable += "<br />" + this.Milk + " milk,";
    OutputTable += "<br /><br /><u>Miscellaneous items</u>";
    OutputTable += "<br />" + this.Misc + "</td>";
    OutputTable += "<td class=\"" + this.TableTag + "\"><u>Preparation</u>"; //used css class tag to be able to work with both <td> nodes
    OutputTable += "<br />" + this.Mixing;
    OutputTable += "<br /><br /><u>Baking</u>";
    OutputTable += "<br />" + this.Cooking + "</td></tr></tbody></table><br />";
    
    //used id tags because each link object is unique with reference to the parent
    OutputTable += 'Rate this recipe: <a href=" " id="' + this.GreatTag + '">Great</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=" " id="' + this.OkayTag + '">Just Okay</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=" " id="' + this.EcchTag + '">Ecch!</a><br /><br />';
    
    return OutputTable;
};

//constructs mini cake card table
Cake.prototype.showMeCakeMini = function(){
    var OutputTable;
    
    //build an output table to look like a recipe book card mini window use
    OutputTable = "<table><tbody><tr><th colspan=7>" + this.Name + " Recipe</a></th></tr>";
    OutputTable += "<tr><td class=\"" + this.TableTag + "\"><u>Ingredients</u>"; 
    OutputTable += "<br />" + this.Flour + " flour,";
    OutputTable += "<br />" + this.Sugar + " sugar,";
    OutputTable += "<br />" + this.Eggs + " egg(s),";
    OutputTable += "<br />" + this.Flavor + " for flavoring,";
    OutputTable += "<br />" + this.Milk + " milk,";
    OutputTable += "<br /><br /><u>Miscellaneous items</u>";
    OutputTable += "<br />" + this.Misc + "</td>";
    OutputTable += "<td><u>Preparation</u>";
    OutputTable += "<br />" + this.Mixing;
    OutputTable += "<br /><br /><u>Baking</u>";
    OutputTable += "<br />" + this.Cooking + "</td></tr></tbody></table><br />";
    OutputTable += "<input type=\"button\" value=\"Close\" onclick=\"window.close();\">";
    
    return OutputTable;
};

function GetCake(i){
        
    // static data for testing
    switch(i){
        case 0:
            Name = "Vanilla Cake";
            Flour = "1.5c sifted";
            Sugar = "1c";
            Eggs = "2 large";
            Flavor = "1/2tsp Vanilla extract";
            Milk = "1/2c whole";
            Misc = "1/4tsp salt, 1/2c butter, 8in cake pan";
            Mixing = "grease pan with butter, mix items, pour in pan";
            Cooking = "400F for 30-40m";
            break;
        case 1:
            Name = "Chocolate Cake";
            Flour = "1.5c sifted";
            Sugar = "1c";
            Eggs = "2 large";
            Flavor = "1c chocolate chips";
            Milk = "1/2c whole";
            Misc = "1/4tsp salt, 1/2c butter, 8in cake pan";
            Mixing = "grease pan with butter, mix items, pour in pan";
            Cooking = "400F for 30-40m";
            break;
        case 2:
            Name = "Cheese Cake";
            Flour = "1.5c sifted";
            Sugar = "1c";
            Eggs = "2 large";
            Flavor = "1c whipped cheese";
            Milk = "1/2c whole";
            Misc = "1/4tsp salt, 1/2c butter, 8in cake pan";
            Mixing = "grease pan with butter, mix items, pour in pan";
            Cooking = "400F for 30-40m";
            break;
        default:
            Name = "Birthday Cake";
            Flour = "1.5c sifted";
            Sugar = "1c";
            Eggs = "2 large";
            Flavor = "1c sprinkles";
            Milk = "1/2c whole";
            Misc = "1/4tsp salt, 1/2c butter, 8in cake pan";
            Mixing = "grease pan with butter, mix items, pour in pan";
            Cooking = "400F for 30-40m";
            break;
    }
    
    
    /*
    //dialog recipe input
    Name = window.prompt("What is this recipe's name?", "Vannilla Cake");
    Flour = window.prompt("How much flour do you need?", "1.5c sifted");
    Sugar = window.prompt("How much sugar do you need?", "1c");
    Eggs = window.prompt("How many eggs do you need?", "2 large");
    Flavor = window.prompt("How much (and what type of) flavoring do you use?", "1/2tsp Vanilla extract");
    Milk = window.prompt("How much milk do you need?", "1/2c whole");
    Misc = window.prompt("What miscellaneous items are needed?", "1/4tsp salt, 1/2c butter, 8in cake pan");
    Mixing = window.prompt("What are the mixing/prep directions?", "grease pan with butter, mix items, pour in pan");
    Cooking = window.prompt("What are the cooking directions?", "400F for 30-40m");
    */
    
    //returns new cake object using default or input values
    return new Cake(Name, Flour, Sugar, Eggs, Flavor, Milk, Misc, Mixing, Cooking);
}

//array that will be filled with cake objects soon
var CakeBook = new Array(NumberOfCakes);

//get cake recipes, display cake recipes, set event handlers
for (var i = 0; i < CakeBook.length; i++){
    CakeBook[i] = GetCake(i);
    document.body.insertAdjacentHTML("beforeend", CakeBook[i].showMeCakeMain());
    
    //apparently, closures/self invoking functions are needed in JS to capture the state of i for each event handler.
    //This is the strangest quirk I've seen in JS so far, having to use this pattern to set multiple event handlers
    //in a loop is convoluted versus Java.
    
    //card links (ratings, popup card)
    document.getElementById(CakeBook[i].GreatTag).onclick = (function(x){
        //self invoking function captures i value (as x) and returns a nested event handler function that uses the captured value (as x)
        return function(){
            //get all the <td> items with a class name matching the objects TableTag var which was assigned to the <td> items in .showMeCake()
            var Item = document.getElementsByClassName(CakeBook[x].TableTag);
            
            //loop through each <td> item and change the background color
            for (var z = 0; z < Item.length; z++){
                Item[z].style.backgroundColor = "gold";
            }
            
            //finally figured out why this is still needed, even though it doesn't prevent the browser from following a link
            //if it's listed in the href, with a blank href it prevents the browser from reloading the current page and erasing
            //all the changes to the page that have been made with JS.
            return false;
        }; 
    })(i);
    
    //see comments above, works the same
    document.getElementById(CakeBook[i].OkayTag).onclick = (function(x){
        return function(){
            var Item = document.getElementsByClassName(CakeBook[x].TableTag);
            for (var z = 0; z < Item.length; z++){
                Item[z].style.backgroundColor = "silver";
            }
            return false;
        };
    })(i);
    
    //see comments above, works the same
    document.getElementById(CakeBook[i].EcchTag).onclick = (function(x){
        return function(){
            var Item = document.getElementsByClassName(CakeBook[x].TableTag);
            for (var z = 0; z < Item.length; z++){
                Item[z].style.backgroundColor = "brown";
            }
            return false;
        }; 
    })(i);
    
    //open popup with recipe when user clicks title
    document.getElementById(CakeBook[i].LinkTag).onclick = (function(x){
        return function(){
            var NewWin = window.open("", "cakepage", "width=550,height=350");
            
            //get Mini Cake Card table and add headers/tags
            var MiniCake = CakeBook[x].showMeCakeMini();
            var NewHtml = "<HTML><HEAD><title>Mini Cake Card</title><link rel=\"stylesheet\" type=\"text/css\" href=\"style_mini.css\"></HEAD><BODY>" + MiniCake + "</BODY></HTML>";
            
            //open new doc, write NewHtml code, close doc
            NewWin.document.open("text/html");
            NewWin.document.write(NewHtml);
            NewWin.document.close();
            }
    })(i);
            
}
